# IMHO-NAACL2019

Data for NAACL 2019 paper
# IMHO Fine-Tuning Improves Claim Detection

Claims are the central component of an argument. Detecting claims across different domains or data sets can often be challenging due
to their varying conceptualization. We propose to alleviate this problem by fine tuning  a language model using a Reddit corpus of 5.5 
million opinionated claims. These claims are self-labeled by their authors using the internet acronyms IMO/IMHO (in my (humble) opinion). 
Empirical results show that using this approach improves the state of art performance across four benchmark argumentation data
sets by an average of 4 absolute F1 points in claim detection. As these data sets include diverse domains such 
as social media and student essays this improvement demonstrates the robustness of fine-tuning on this novel corpus.

Email:
Tuhin Chakrabarty
(tc2896@columbia.edu)


If you use the idea or data
Please cite us 

@inproceedings{chakrabarty-etal-2019-imho,
    title = "{IMHO} Fine-Tuning Improves Claim Detection",
    author = "Chakrabarty, Tuhin  and
      Hidey, Christopher  and
      McKeown, Kathy",
    booktitle = "Proceedings of the 2019 Conference of the North {A}merican Chapter of the Association for Computational Linguistics: Human Language Technologies, Volume 1 (Long and Short Papers)",
    month = jun,
    year = "2019",
    address = "Minneapolis, Minnesota",
    publisher = "Association for Computational Linguistics",
    url = "https://www.aclweb.org/anthology/N19-1054",
    pages = "558--563",
}

